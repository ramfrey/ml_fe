#!/usr/bin/env python
# coding: utf-8

# In[32]:


def oxidation_state_test(formula,which_states):
    #which states can either be 'common', 'icsd', 'all'
    import pymatgen as mg
    from itertools import product
    comp=mg.Composition(formula).element_composition.as_dict()
    ox_states={}
    list_ox_states=[]
    key_list=list(comp.keys())
    for key in key_list:
        ox_states[key]=mg.core.periodic_table.Element(key).icsd_oxidation_states
        if which_states=='common':
            list_ox_states.append(mg.core.periodic_table.Element(key).common_oxidation_states)
        elif which_states=='icsd':
            list_ox_states.append(mg.core.periodic_table.Element(key).icsd_oxidation_states)
        elif which_states=='all':
            list_ox_states.append(mg.core.periodic_table.Element(key).oxidation_states)
        else:
            raise ValueError('%s is not a valid keyword, the options are: common, icsd, all'%(which_states))
    new_list=list(product(*list_ox_states))
    test=False
    for ii in new_list:
        total_charge=0
        for jj in range(len(ii)):
            total_charge+=ii[jj]*comp[key_list[jj]]
        if total_charge==0:
            test=True
    return test
            

