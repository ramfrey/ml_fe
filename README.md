# ML_FE
#### This repository contains python code for the prediction of new ferroelectrics by machine learning.
#### How to use it:
The main script is "machine_learning_FE.py" which takes a list of ternary or quaternary composition spaces as input (see test_file). 
Run the "machine_learning_FE.py" script and enter the file name of your input file when prompted. 
For each composition space a mesh of stochimetries (A1B1C1D1,...,A9B9C9D9) is generated and for each of these compositions machine learning models predict formation energy, insulating/metallic behaviour, polar/non-polar crystal structure and the pointgroup. All prediction results are saved in a JSON file named "filename_all_results.json". Compositions that are predicted to be potential ferroelectrics (stable, insulating, polar), are listed together with their predicted point-group in the file "filename_candidates.json".

