#!/usr/bin/env python
# coding: utf-8

# In[4]:


############################
#README:
#this script will take a list of composition spaces as input (comma separated)
#example:
# Bi,Fe,O
# Bi,Sr,O
# ...
#you will be prompted to enter the filename of that list,
#which will be used as prefix for the output files 
#(so the file name shouldnt have an extension like .txt)
#
# CONFIGURATION:
## the file elementsnew.xlsx must be in the same folder as this script
# there has to be the subfolder "my_functions" with the oxidation_state_test script
# there has to be the subfolder "Classifiers" containing the already trained classifiers
############################



filename=input('please enter the filename of the input text file, which will also be the name of the .json output files :')

import numpy as np
import pymatgen as mg
import os
import pandas as pd
from itertools import product,combinations,permutations
from joblib import dump, load
import re


# In[5]:


import sys
sys.path.append("my_functions")
import oxidation_state_test as o


# In[6]:


file=np.loadtxt(r'%s'%(filename),delimiter=',',dtype=str)
composition_list=file.tolist()
print(composition_list)


# # Define and initialize class for feature generation
# #### class to create features from properties of the elements (listed in elementsnew file)
# currently uses average, difference, maximum, minimum, and sum of the elemental properties 

# In[7]:


class Vectorize_Formula_all:

	def __init__(self):
		elem_dict = pd.read_excel(r'elementsnew.xlsx') # CHECK NAME OF FILE 
		self.element_df = pd.DataFrame(elem_dict) 
		self.element_df.set_index('Symbol',inplace=True)
		self.column_names = []
		for string in ['avg','diff','max','min','sum']:
			for column_name in list(self.element_df.columns.values):
				self.column_names.append(string+'_'+column_name)


	def get_features(self, formula):
		try:
			fractional_composition = mg.Composition(formula).fractional_composition.as_dict()
			element_composition = mg.Composition(formula).element_composition.as_dict()
			avg_feature = np.zeros(len(self.element_df.iloc[0]))
			sum_feature = np.zeros(len(self.element_df.iloc[0]))
			for key in fractional_composition:
				try: 
					avg_feature += self.element_df.loc[key].values * fractional_composition[key] 
					diff_feature = self.element_df.loc[list(fractional_composition.keys())].max()-self.element_df.loc[list(fractional_composition.keys())].min()
					sum_feature += self.element_df.loc[key].values * element_composition[key] 
				except Exception as e: 
					print('The element:', key, 'from formula', formula,'is not currently supported in our database')
					return np.array([np.nan]*len(self.element_df.iloc[0])*4)
			max_feature = self.element_df.loc[list(fractional_composition.keys())].max()
			min_feature = self.element_df.loc[list(fractional_composition.keys())].min()          
			features = np.concatenate([avg_feature, diff_feature, np.array(max_feature), np.array(min_feature),sum_feature])
			features=features.transpose()
			return features
		except:
			print('There was an error with the Formula: '+ formula + ', this is a general exception with an unkown error')
			return [np.nan]*len(self.element_df.iloc[0])*4


# In[8]:


# initialize feature generation class
gf=Vectorize_Formula_all()


# # load algorithms

# In[9]:


rfc = load('Classifiers/rfc.joblib')
brfc_polar=load('Classifiers/brfc_polar.joblib')
brfc_merged=load('Classifiers/brfc_merged.joblib')
brfc_bandgap=load('Classifiers/brfc_bandgap.joblib')


# In[10]:


points=['3m','4mm','6','6mm','merged','mm2']


# # define prediction algorithms/decision trees
# ## currently decision_tree_merged is being used
# functions are defined to predict the compositions on the convex hull (or slightly above) using formation energy prediction and then of those "stable" compositions it is predicted if they are polar or not and if they are polar which point group they have 

# In[11]:


def hull(formula_list, rfc,maxi):
    entries=[]
    complist=[]
    for formula in formula_list:
        comp = mg.Composition(formula).fractional_composition.as_dict() #fractional composition needed for pymatgen convex hull calculation
        feat=gf.get_features(formula).reshape((1,-1))
        entry=mg.entries.computed_entries.ComputedEntry(comp,rfc.predict(feat)[0])#*sum(list(mg.Composition(formula).as_dict().values())))
        entries.append(entry)
    for key in list(comp.keys()):
        #print(key)
        entries.append(mg.entries.computed_entries.ComputedEntry(mg.Composition(key),0))

    phase_diagram=mg.analysis.phase_diagram.PhaseDiagram(entries)
    hull_values=[]
    #print(len(entries))
    for ii in entries:
        hull_values.append(phase_diagram.get_e_above_hull(ii))
            
    return hull_values


# In[12]:


def decision_tree_merged(formula):
    feat=gf.get_features(formula).reshape((1,-1))
    if brfc_polar.predict(feat)==1:
        merged=brfc_merged.predict(feat)
        probs=brfc_merged.predict_proba(feat)
        return merged , probs
    else:
        return None , None


# In[13]:


def band_gap_check(formula):
    feat=gf.get_features(formula).reshape((1,-1))
    if brfc_bandgap.predict(feat)==1:
        return True


# # run decision tree on all entries in phase space
#  applies convex hull and pointgroup prediction to all the compositions and returns a list with the ones 
#  predicted to be polar and their pointgroups

# In[ ]:


results={}
candidates={}
for composition in composition_list:
    phase_space=composition
    print(phase_space)
    name_phase_space='_'.join(phase_space)
    #max number of one element:
    maxi=9

    list1=[]

    for ii in range(len(phase_space)):
        list1.append(list(range(1,1+maxi)))

    new_list=list(product(*list1))

    formulas=dict()
    formula_list_old=[]
    for ii in range(len(phase_space)):
        formulas[phase_space[ii]]=1
    for ii in range(len(new_list)):
        for jj in range(len(phase_space)):
            formulas[phase_space[jj]]=new_list[ii][jj]
        comp = mg.Composition.from_dict(formulas)
        if len(list(comp.as_dict().values()))>=2:
            formula_list_old.append(comp.alphabetical_formula)
    complist=[]
    formula_list=[]
    for formula in formula_list_old:
        comp = mg.Composition(formula).fractional_composition.as_dict() #fractional composition needed for pymatgen convex hull calculation
        if comp not in complist:
            formula_list.append(formula)
        complist.append(comp)
        

    res=[]
    hull_list=hull(formula_list, rfc,maxi)
    hull_list=hull_list[:-len(list(comp.keys()))]
    """if not os.path.exists('Vasp_results/%s'%name_phase_space):
    os.makedirs('Vasp_results/%s'%name_phase_space)
    with open('Vasp_results/%s/hull_list.txt'% name_phase_space, 'w') as f:
    for item in hull_list:
        f.write("%s\n" % item)"""
    ox_state=[]
    polarity=[]
    point=[]
    point_probs=[]
    band_gap=[]
    for ii in formula_list:
        if o.oxidation_state_test(ii,'common'):
            ox_state.append('common')
        elif o.oxidation_state_test(ii,'icsd'):
            ox_state.append('icsd')
        elif o.oxidation_state_test(ii,'all'):
            ox_state.append('all')
        else:
            ox_state.append('not possible')
            
        feat=gf.get_features(ii).reshape((1,-1))
        if brfc_polar.predict(feat)==1:
            polarity.append('polar')
        else:
            polarity.append('non-polar')
        
        point.append(brfc_merged.predict(feat)[0])
        point_probs.append(brfc_merged.predict_proba(feat))
        
        if band_gap_check(ii):
            band_gap.append('insulator')
        else:
            band_gap.append('metallic')
            
            
    assert len(formula_list)==len(hull_list)    
    assert len(formula_list)==len(ox_state)
    assert len(formula_list)==len(polarity)
    assert len(formula_list)==len(band_gap)
    assert len(formula_list)==len(point)
    dummydict={}
    res=[]
    for jj in range(len(formula_list)):
        dummyprobs={}
        for kk in range(len(points)):
            dummyprobs[points[kk]]=point_probs[jj][0][kk]
        dummydict[formula_list[jj]]={'e_above_hull': hull_list[jj],'oxstate': ox_state[jj],'band_gap': band_gap[jj],
                                     'polarity': polarity[jj], 'pointgroup': point[jj],'point group probabilities': dummyprobs}
        
        highest=int(max(re.findall(r'\d+', formula_list[jj])))
        #print(hull_list[jj],ox_state[jj],polarity[jj],band_gap[jj])
        if (highest<= 9 and hull_list[jj]<= 0.15 and (ox_state[jj]== 'common' or ox_state[jj]=='icsd') and polarity[jj]=='polar' and band_gap[jj]=='insulator'):
            if point[jj]=='merged':
                res.append((point[jj],formula_list[jj],['1','2','3','4','m']))
            else:
                res.append((point[jj],formula_list[jj],[point[jj]]))

    results[name_phase_space]=dummydict
    if res!=[]:
        candidates[name_phase_space]=res
    else:
        candidates[name_phase_space]='no candidates found, see _all_results file for more details)'

import json
with open('%s_all_results.json'%(filename), 'w') as fp:
    json.dump(results, fp,indent=4)
with open('%s_candidates.json'%(filename), 'w') as fp:
    json.dump(candidates, fp,indent=4)
print('for the following composition spaces candidates were found: \n')
for ii in range(len(list(candidates.keys()))):
    if type(candidates[list(candidates.keys())[ii]]) is not str:
        print(list(candidates.keys())[ii],'\n')
print('results were written to files: \n %s_all_results.json \n %s_candidates.json'%(filename,filename))


# In[ ]:




